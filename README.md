# How install
In your container with apt with user root

```bash 
apt update && apt install -y git \
&& git clone https://gitlab.com/david-piatek/container-tools.git /root/.tools \
&& bash /root/.tools/install.sh
```