#!/usr/bin/env bash

apt update && apt install -y vim zsh screen;
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
echo "source /root/.tools/.zshrc.ext" >> /root/.zshrc
echo "Finish"
